### List of open questions.

The following is the list of problems that I have thought naively. Please add your own also. Maybe we should categorize them at some point.
* Characterizing circuits of algebraic matroid of rank 2 matrices.
* Characterization of rank 2 symmetric matrices.
* Possible inertia (signature) of a completion of partial symmetric matrices.
  - Conjecture : For full-rank typical graphs, all possible inertias correspond to possible proper bicolorings of its complement graph (Conjecture 4.6 of BBL20)
  
* What is a tropicalization of a variety of rank 2 (symmetric) matrices?
  - Understand tropical convex sets.
  
* Numerical algorithm to compute typical ranks?
* Can we find typical ranks without using linear algebra (like Schur complement)?
* Relationship with secant varieties?
* Do coordinate projections of rank sets of tropical matrices always have the same dimension for different notions of rank (Barvinok, Kapranov, tropical rank)?

This is a bit farther off from matrix completion:
* Can combinatorics and tropical geometry reprove the known results about defective secants of Segre-Veronese varieties?  Can this strategy go further?
* What are the properties of "secant matroids"?
